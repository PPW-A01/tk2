from django.apps import apps
from django.test import TestCase
from otentikasi.apps import OtentikasiConfig

class OtentikasiConfigTest(TestCase):
    def test_apps(self):
        self.assertEqual(OtentikasiConfig.name, 'otentikasi')
        self.assertEqual(apps.get_app_config('otentikasi').name, 'otentikasi')
