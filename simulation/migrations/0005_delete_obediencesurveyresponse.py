# Generated by Django 3.1.4 on 2021-01-02 12:09

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('simulation', '0004_remove_obediencesurveyresponse_haha'),
    ]

    operations = [
        migrations.DeleteModel(
            name='ObedienceSurveyResponse',
        ),
    ]
