from django.contrib import admin
from .models import ObedienceSurveyResponse

# Register your models here.

admin.site.register(ObedienceSurveyResponse)