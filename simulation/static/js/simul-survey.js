$(document).ready(() => {
    $.ajax({
        type: 'GET',
        url: '/simulation/score/',
        success: data => {
            $('.response-container').empty();

            var dataCount = 0;
            var maskTotal = 0.0;
            var pdTotal = 0.0;
            var sntzTotal = 0.0;
            var averageScore = 0.0;

            data.map(response => {

                dataCount++;
                maskTotal += response.fields.mask_obedience;
                pdTotal += response.fields.PD_obedience;
                sntzTotal += response.fields.sntz_obedience;
                averageScore = (response.fields.mask_obedience + response.fields.PD_obedience + response.fields.sntz_obedience) / 3

                $('.response-container').append(
                    `
                    <div>
                        <div class="forms-card response-button-container" style="text-align: center; margin-top: 2rem; padding: 2rem;">
                            <h3><strong>Response #${ dataCount }</strong> - Average score: ${ averageScore.toFixed(1) }% </h3>
                            <button type="button" class="survey-button show-hide-response-button" value="hidden">Show Response</button>
                        </div>
                        
                        <div class="forms-card response" style="text-align: center; margin-top: 2rem; padding: 2rem; display : none">
                            <h3><strong>Response #${ dataCount }</strong> - Average score: ${ averageScore.toFixed(1) }% </h3>
                            <button type="button" class="survey-button show-hide-response-button" value="shown">Hide Response #${ dataCount }</button>
                            <h3 style="margin-top: 2rem; margin-bottom:0">Score:</h3> <br>
                            <div class="container">
                                <div class="row percentage-list" style="margin-top: -2rem; margin-bottom: 0">
                                    <div class="col percentage-item">
                                        <div class="card-body">
                                            <p class="percentage-number">${ response.fields.mask_obedience }<span class="percentage-sign">/100</span></p>
                                            <p class="percentage-detail">of Wearing<br>Masks Properly <br> in Public Places</p>
                                        </div>
                                    </div>

                                    <div class="col percentage-item">
                                        <div class="card-body">
                                            <p class="percentage-number">${ response.fields.PD_obedience }<span class="percentage-sign">/100</span></p>
                                            <p class="percentage-detail">of Being Aware of<br>and Practicing<br>Physical Distancing</p>
                                        </div>
                                    </div>

                                    <div class="col percentage-item">
                                        <div class="card-body">
                                            <p class="percentage-number">${ response.fields.sntz_obedience }<span class="percentage-sign">/100</span></p>
                                            <p class="percentage-detail">of Washing or<br> Sanitizing Hands and<br>Disinfecting Regularly</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h3>Response:</h3><br>

                            <div class="forms-question answer" style="text-align: left;">What kind of mask do you usually wear in public?</div>
                            <div class="forms-answer" style="text-align: left; margin: 0.5em 0 1.5em 2em;">${ response.fields.mask_type }</div>

                            <div class="forms-question answer" style="text-align: left;">Tell us about your habits when using a mask in public</div>
                            <div class="forms-answer" style="text-align: left; margin: 0.5em 0 1.5em 2em;">${ response.fields.mask_behavior }</div>

                            <div class="forms-question answer" style="text-align: left;">Tell us about your experience when conducting physical distancing regarding the COVID-19 pandemic</div>
                            <div class="forms-answer" style="text-align: left; margin: 0.5em 0 1.5em 2em;">${ response.fields.PD_experience }</div>

                            <div class="forms-question answer" style="text-align: left;">When you go out, in what type of public area do you spend the most time?</div>
                            <div class="forms-answer" style="text-align: left; margin: 0.5em 0 1.5em 2em;">${ response.fields.PD_area_space }</div>

                            <div class="forms-question answer" style="text-align: left;">Do you wash your hands regularly?</div>
                            <div class="forms-answer" style="text-align: left; margin: 0.5em 0 1.5em 2em;">${ response.fields.sntz_wash_hand }</div>

                            <div class="forms-question answer" style="text-align: left;">With which type of soap do you usually wash your hands regularly?</div>
                            <div class="forms-answer" style="text-align: left; margin: 0.5em 0 1.5em 2em;">${ response.fields.sntz_soap_type }</div>

                            <div class="forms-question answer" style="text-align: left;">How long in seconds do you usually wash your hands</div>
                            <div class="forms-answer" style="text-align: left; margin: 0.5em 0 1.5em 2em;">${ response.fields.sntz_wash_time }</div>

                            <div class="forms-question answer" style="text-align: left;">In what occasions of these do you usually wash your hands?</div>
                            <div class="forms-answer" style="text-align: left; margin: 0.5em 0 1.5em 2em;">${ response.fields.sntz_occasions }</div>

                            <div class="forms-question answer" style="text-align: left;">If you don\'t have an access to wash your hands, do you usually apply sanitizer to them?</div>
                            <div class="forms-answer" style="text-align: left; margin: 0.5em 0 1.5em 2em;">${ response.fields.sntz_hsanitizer }</div>

                            <div class="forms-question answer" style="text-align: left;">What percentage of alcohol does your hand sanitizer contain? (0 if it does not contain alcohol or you don’t usually use hand sanitizer)</div>
                            <div class="forms-answer" style="text-align: left; margin: 0.5em 0 1.5em 2em;">${ response.fields.sntz_alcohol_percentage }</div>

                            <div class="forms-question answer" style="text-align: left;">When you go home from out, do you usually disinfect carrying, bag or things?</div>
                            <div class="forms-answer" style="text-align: left; margin: 0.5em 0 1.5em 2em;">${ response.fields.sntz_disinfect }</div>

                        </div> 
                    </div>`
                );
            });

            var maskPercentage = "0.0";
                var pdPercentage = "0.0";
                var sntzPercentage = "0.0";

                if (dataCount != 0){
                    maskPercentage = (maskTotal/dataCount).toFixed(1);
                    pdPercentage = (pdTotal/dataCount).toFixed(1);
                    sntzPercentage = (sntzTotal/dataCount).toFixed(1);
                }
                
                $("#mask-percentage").html(`${ maskPercentage }<span class='percentage-sign'>%</span>`);
                $("#pd-percentage").html(`${ pdPercentage }<span class='percentage-sign'>%</span>`);
                $("#sntz-percentage").html(`${ sntzPercentage }<span class='percentage-sign'>%</span>`);

        }
    });

    $('#survey-submit-button').on('click', e => {
        e.preventDefault();

        let csrfToken = $('input[name = csrfmiddlewaretoken]').val();
        var maskBehaviorList = new Array();
        var washingHandsOccasionsList = new Array();

        $("input[name='mask_behavior_point']:checked").each(function(){
            maskBehaviorList.push($(this).val());
        });

        $("input[name='sanitize_occasions_point']:checked").each(function(){
            washingHandsOccasionsList.push($(this).val());
        });

        $.ajax({
            type: 'POST',
            url: '/simulation/score/',
            data: {
                csrfmiddlewaretoken: csrfToken,
                mask_type_point: $("input[name='mask_type_point']:checked").val(),
                mask_behavior_point: JSON.stringify(maskBehaviorList),
                pd_exp_point: $("input[name='pd_exp_point']:checked").val(),
                pd_area_crowdedness_point: $("input[name='pd_area_crowdedness_point']:checked").val(),
                
                pd_area_openness_point: $("input[name='pd_area_openness_point']:checked").val(),
                sanitize_wash_hand_point: $("input[name='sanitize_wash_hand_point']:checked").val(),
                sanitize_soap_type_point: $("input[name='sanitize_soap_type_point']:checked").val(),
                sanitize_wash_time: $('#id_sanitize_wash_time').val(),

                sanitize_occasions_point: JSON.stringify(washingHandsOccasionsList),
                
                sanitize_hsanitizer_point: $("input[name='sanitize_hsanitizer_point']:checked").val(),
                sanitize_alcohol_percentage: $('#id_sanitize_alcohol_percentage').val(),
                sanitize_disinfect_point: $("input[name='sanitize_disinfect_point']:checked").val()
            },
            success: data => {
                $('#main-survey').find('input[type=checkbox]:checked').removeAttr('checked');
                $('#main-survey').find('input[type=radio]:checked').removeAttr('checked');
                $('#main-survey').find('input[type=number]').val('');
                $('.response-container').empty();

                var dataCount = 0;
                var maskTotal = 0.0;
                var pdTotal = 0.0;
                var sntzTotal = 0.0;
                var averageScore = 0.0;

                data.map(response => {

                    dataCount++;
                    maskTotal += response.fields.mask_obedience;
                    pdTotal += response.fields.PD_obedience;
                    sntzTotal += response.fields.sntz_obedience;
                    averageScore = (response.fields.mask_obedience + response.fields.PD_obedience + response.fields.sntz_obedience) / 3

                    $('.response-container').append(
                        `
                        <div>
                            <div class="forms-card response-button-container" style="text-align: center; margin-top: 2rem; padding: 2rem; display : none">
                                <h3><strong>Response #${ dataCount }</strong> - Average score: ${ averageScore.toFixed(1) }% </h3>
                                <button type="button" class="survey-button show-hide-response-button" value="hidden">Show Response</button>
                            </div>
                            
                            <div class="forms-card response" id="response_${ dataCount }" style="text-align: center; margin-top: 2rem; padding: 2rem;">
                                <h3><strong>Response #${ dataCount }</strong> - Average score: ${ averageScore.toFixed(1) }% </h3>
                                <button type="button" class="survey-button show-hide-response-button" value="shown">Hide Response #${ dataCount }</button>
                                <h3 style="margin-top: 2rem; margin-bottom:0">Score:</h3> <br>
                                <div class="container">
                                    <div class="row percentage-list" style="margin-top: -2rem; margin-bottom: 0">
                                        <div class="col percentage-item">
                                            <div class="card-body">
                                                <p class="percentage-number">${ response.fields.mask_obedience }<span class="percentage-sign">/100</span></p>
                                                <p class="percentage-detail">of Wearing<br>Masks Properly <br> in Public Places</p>
                                            </div>
                                        </div>

                                        <div class="col percentage-item">
                                            <div class="card-body">
                                                <p class="percentage-number">${ response.fields.PD_obedience }<span class="percentage-sign">/100</span></p>
                                                <p class="percentage-detail">of Being Aware of<br>and Practicing<br>Physical Distancing</p>
                                            </div>
                                        </div>

                                        <div class="col percentage-item">
                                            <div class="card-body">
                                                <p class="percentage-number">${ response.fields.sntz_obedience }<span class="percentage-sign">/100</span></p>
                                                <p class="percentage-detail">of Washing or<br> Sanitizing Hands and<br>Disinfecting Regularly</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <h3>Response:</h3><br>

                                <div class="forms-question answer" style="text-align: left;">What kind of mask do you usually wear in public?</div>
                                <div class="forms-answer" style="text-align: left; margin: 0.5em 0 1.5em 2em;">${ response.fields.mask_type }</div>

                                <div class="forms-question answer" style="text-align: left;">Tell us about your habits when using a mask in public</div>
                                <div class="forms-answer" style="text-align: left; margin: 0.5em 0 1.5em 2em;">${ response.fields.mask_behavior }</div>

                                <div class="forms-question answer" style="text-align: left;">Tell us about your experience when conducting physical distancing regarding the COVID-19 pandemic</div>
                                <div class="forms-answer" style="text-align: left; margin: 0.5em 0 1.5em 2em;">${ response.fields.PD_experience }</div>

                                <div class="forms-question answer" style="text-align: left;">When you go out, in what type of public area do you spend the most time?</div>
                                <div class="forms-answer" style="text-align: left; margin: 0.5em 0 1.5em 2em;">${ response.fields.PD_area_space }</div>

                                <div class="forms-question answer" style="text-align: left;">Do you wash your hands regularly?</div>
                                <div class="forms-answer" style="text-align: left; margin: 0.5em 0 1.5em 2em;">${ response.fields.sntz_wash_hand }</div>

                                <div class="forms-question answer" style="text-align: left;">With which type of soap do you usually wash your hands regularly?</div>
                                <div class="forms-answer" style="text-align: left; margin: 0.5em 0 1.5em 2em;">${ response.fields.sntz_soap_type }</div>

                                <div class="forms-question answer" style="text-align: left;">How long in seconds do you usually wash your hands</div>
                                <div class="forms-answer" style="text-align: left; margin: 0.5em 0 1.5em 2em;">${ response.fields.sntz_wash_time }</div>

                                <div class="forms-question answer" style="text-align: left;">In what occasions of these do you usually wash your hands?</div>
                                <div class="forms-answer" style="text-align: left; margin: 0.5em 0 1.5em 2em;">${ response.fields.sntz_occasions }</div>

                                <div class="forms-question answer" style="text-align: left;">If you don\'t have an access to wash your hands, do you usually apply sanitizer to them?</div>
                                <div class="forms-answer" style="text-align: left; margin: 0.5em 0 1.5em 2em;">${ response.fields.sntz_hsanitizer }</div>

                                <div class="forms-question answer" style="text-align: left;">What percentage of alcohol does your hand sanitizer contain? (0 if it does not contain alcohol or you don’t usually use hand sanitizer)</div>
                                <div class="forms-answer" style="text-align: left; margin: 0.5em 0 1.5em 2em;">${ response.fields.sntz_alcohol_percentage }</div>

                                <div class="forms-question answer" style="text-align: left;">When you go home from out, do you usually disinfect carrying, bag or things?</div>
                                <div class="forms-answer" style="text-align: left; margin: 0.5em 0 1.5em 2em;">${ response.fields.sntz_disinfect }</div>

                            </div> 
                        </div>`
                    );
                });

                var maskPercentage = "0.0";
                var pdPercentage = "0.0";
                var sntzPercentage = "0.0";

                if (dataCount != 0){
                    maskPercentage = (maskTotal/dataCount).toFixed(1);
                    pdPercentage = (pdTotal/dataCount).toFixed(1);
                    sntzPercentage = (sntzTotal/dataCount).toFixed(1);
                }

                var alertMsg = `Response #${ dataCount } added. Thank you for filling out our survey! :D`;
                alert(alertMsg);
                
                $("#mask-percentage").html(`${ maskPercentage }<span class='percentage-sign'>%</span>`);
                $("#pd-percentage").html(`${ pdPercentage }<span class='percentage-sign'>%</span>`);
                $("#sntz-percentage").html(`${ sntzPercentage }<span class='percentage-sign'>%</span>`);

                $(".response").css("display", "none");
                $(".response-button-container").css("display", "block");

                var addedResponse = $(`#response_${ dataCount }`);
                var addedResponseButton = addedResponse.prev();

                addedResponseButton.css("display", "none");
                addedResponse.css("display", "block");
                $('html, body').animate({
                    scrollTop: $(`#response_${ dataCount }`).offset().top
                }, 200);

            },
            error: data => {
                var alertMsg = "Failed to fill out the survey. Message:";
                $.each(JSON.parse(data.responseJSON),function(index,value){ 
                    $.each(value, function(){
                        alertMsg = alertMsg.concat("\n -", this.message);
                    });
                });
                alert(alertMsg);
            }
        });
    });

    $("#show-hide-survey-button").click(function(){
        let button = $("#show-hide-survey-button");
        let surveyContainer = $("#main-survey");
        if (button.val() == "hidden"){
            button.val("shown");
            button.html("Hide Survey");
            surveyContainer.css("display", "block");
        }
        else { //if shown
            button.val("hidden");
            button.html("Show Survey")
            surveyContainer.css("display", "none");
        }
    });

    $(document).on('click', '.show-hide-response-button', function(){
        let button = $(this);
        console.log(button);
        let buttonParent = button.parent();
        if (button.val() == "hidden"){
            let targetResponse = buttonParent.next();
            $(".response").css("display", "none");
            $(".response-button-container").css("display", "block");
            buttonParent.css("display", "none");
            targetResponse.css("display", "block");
            $('html, body').animate({
                scrollTop: targetResponse.offset().top
            }, 200);
        }
        else { //if shown
            let targetResponse = buttonParent.prev();
            buttonParent.css("display", "none");
            targetResponse.css("display", "block");
        }
    });

    $("#show-hide-survey-button").hover(
        function(){
            $(this).css("background-color", "#c8fff5");
        }, 
        function(){
            $(this).css("background-color", "transparent");
        }
    );

    $(".back-button").hover(
        function(){
            $(this).css("background-color", "#c8fff5");
        }, 
        function(){
            $(this).css("background-color", "transparent");
        }
    );
});