$(document).ready(() => {
    $("#simulate-test").keyup( 
        function(){
            var testValue = $("#simulate-test").val();

            if ($.isNumeric(testValue) && !testValue.isNaN){
                $("#test-result").empty();
                if (testValue > 200000){
                    $("#simulate-test").val("200000");
                    testValue = 200000;
                    alert("Simulation failed. The maximum amount of test count is 200,000 tests.");
                }
                else if (testValue < 0){
                    $("#simulate-test").val("0");
                    testValue = 0;
                    alert("Simulation failed. The minimum amount of test count is 0 tests.");
                }

                var resPercentage = 14.0 - testValue / 30000 * 0.1;
                var result = Math.ceil(resPercentage / 100 * testValue);
                $("#test-result").append(
                    `
                    <h3 class="forms-card" style="text-align: center; margin-top: 3rem; padding: 0rem;">
                        Out of <span style="background-color: white;">${ testValue }</span> taken tests, 
                        there would be: <br> <br>
                        <span style="font-size: 4rem;">${ result }</span>
                        <br> <br>
                        New Cases <br><br>
                    </h3>`
                );

            }
            else {
                alert("Invalid input. Please try again with numbers.")
            }
        }
    );

    $("#take-survey-button").hover(
        function(){
            $(this).css("background-color", "#c8fff5");
        }, 
        function(){
            $(this).css("background-color", "transparent");
        }
    );
});