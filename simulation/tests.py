from django.test import TestCase, Client
from django.urls import resolve

from .views import *
from .models import *

# Create your tests here.

class SimulationGeneralTesting(TestCase):
    def test_simulation_url_exists(self):
        response = Client().get('/simulation/')
        self.assertEquals(response.status_code, 200)

    def test_simulation_survey_url_exists(self):
        user = Client().post('/register/', {
            'username': 'dananakbar',
            'email': 'danan.maulidan@ui.ac.id',
            'password1': 'thomasandhisfriends',
            'password2': 'thomasandhisfriends'
            }, follow = True)
        user.client.login(username='dananakbar', password='thomasandhisfriends')
        response = user.client.get('/simulation/survey/')
        self.assertEquals(response.status_code, 200)

    def test_simulation_score_url_found(self):
        user = Client().post('/register/', {
            'username': 'dananakbar',
            'email': 'danan.maulidan@ui.ac.id',
            'password1': 'thomasandhisfriends',
            'password2': 'thomasandhisfriends'
            }, follow = True)
        user.client.login(username='dananakbar', password='thomasandhisfriends')

        response = user.client.post('/simulation/score/', data={
            'mask_type_point': '3-ply cloth mask',
            'mask_behavior_point': '["I wear masks loosely","I don\'t really cover my nose or my mouth with my mask"]',
            'pd_exp_point': 'I usually distance myself from strangers in public and I\'m pretty sure that most of the time, the distance has always been at least 2 meters (6 ft 6 in) away',
            'pd_area_crowdedness_point':"Uncrowded",
            'pd_area_openness_point':"Open area",
            'sanitize_wash_hand_point':'Yes, I wash my hands regularly',
            'sanitize_soap_type_point':'Liquid soap',
            'sanitize_wash_time':30,
            'sanitize_occasions_point':'["After using the restroom","Before eating or preparing food","After contact with animals or pets"]',
            'sanitize_hsanitizer_point':'Yes, I sanitize my hands when I don\'t have a proper access to wash them',
            'sanitize_alcohol_percentage':80,
            'sanitize_disinfect_point':'Yes, I usually disinfect things I bring from outside'
            }, follow=True)
        self.assertEquals(response.status_code, 200)

        response = user.client.post('/simulation/score/', data={
            'mask_type_point': '3-ply cloth mask',
            'mask_behavior_point': '["I wear masks loosely","I don\'t really cover my nose or my mouth with my mask"]',
            'pd_exp_point': 'I usually distance myself from strangers in public and I\'m pretty sure that most of the time, the distance has always been at least 2 meters (6 ft 6 in) away',
            'pd_area_crowdedness_point':"Uncrowded",
            'sanitize_wash_hand_point':'Yes, I wash my hands regularly',
            'sanitize_soap_type_point':'Liquid soap',
            'sanitize_wash_time':30,
            'sanitize_occasions_point':'["After using the restroom","Before eating or preparing food","After contact with animals or pets"]',
            'sanitize_hsanitizer_point':'Yes, I sanitize my hands when I don\'t have a proper access to wash them',
            'sanitize_alcohol_percentage':80,
            'sanitize_disinfect_point':'Yes, I usually disinfect things I bring from outside'
            }, follow=True)
        self.assertEquals(response.status_code, 403)
        
        response = user.client.get('/simulation/score/')
        self.assertEqual(response.status_code, 200)

    def test_simulation_uses_index_template(self):
        response = Client().get('/simulation/')
        self.assertTemplateUsed(response, 'simul-index.html')

    def test_simulation_uses_index_func(self):
        found = resolve('/simulation/')
        self.assertEquals(found.func, index)

    def test_simulation_uses_score_func(self):
        found = resolve('/simulation/score/')
        self.assertEquals(found.func, score)

    def test_form_is_simulated_test_valid(self):
        form_data = {
            'simulated_test' : 200000
        }
        form = TestForm(data=form_data)
        self.assertTrue(form.is_valid)

    def test_form_is_mask_form_valid(self):
        form_data = {
            'mask_type_point': '3-ply cloth mask',
            'mask_behavior_point': 'I don\'t usually wear a mask in public.',
            }
        form = MaskForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_form_is_pd_form_valid(self):
        form_data = {
            'pd_exp_point': 'I usually distance myself from strangers in public and I\'m pretty sure that most of the time, the distance has always been at least 2 meters (6 ft 6 in) away',
            'pd_area_crowdedness_point':"Uncrowded",
            'pd_area_openness_point':"Open area",
            }
        form = PDForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_form_is_sntz_form_valid(self):
        form_data = {
            'sanitize_wash_hand_point':'Yes, I wash my hands regularly',
            'sanitize_soap_type_point':'Liquid soap',
            'sanitize_wash_time':30,
            'sanitize_occasions_point': 'After coughing or sneezing',
            'sanitize_hsanitizer_point':'Yes, I sanitize my hands when I don\'t have a proper access to wash them',
            'sanitize_alcohol_percentage':80,
            'sanitize_disinfect_point':'Yes, I usually disinfect things I bring from outside'
            }
        form = SanitizeForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_model_get_overall(self):
        ObedienceSurveyResponse.objects.create(mask_obedience=100, PD_obedience=20, sntz_obedience=50)
        ObedienceSurveyResponse.objects.create(mask_obedience=0, PD_obedience=40, sntz_obedience=80)

        mask_percentage = get_ob_overall('MASK')
        self.assertEquals(mask_percentage, '50.0')

        pd_percentage = get_ob_overall('PD')
        self.assertEquals(pd_percentage, '30.0')

        sntz_percentage = get_ob_overall('SNTZ')
        self.assertEquals(sntz_percentage, '65.0')

    def test_model_can_create_new_percentage(self):
        ObedienceSurveyResponse.objects.create(mask_obedience=1, PD_obedience=1, sntz_obedience=1)
        OP_count = ObedienceSurveyResponse.objects.all().count()
        self.assertEquals(OP_count, 1)

    def test_views_process_wash_time_point_works(self):
        wash_time = '30'
        wash_time_point = process_wash_time_point(wash_time)
        self.assertEquals(wash_time_point, 1.0)

        wash_time = '10'
        wash_time_point = process_wash_time_point(wash_time)
        self.assertEquals(wash_time_point, 0.5)
    
    def test_views_process_alcohol_percentage_point_works(self):
        alcohol_percentage = 50
        alcohol_percentage_point = process_alcohol_percentage_point(alcohol_percentage)
        self.assertEquals(alcohol_percentage_point, 0.0)

        alcohol_percentage = 70
        alcohol_percentage_point = process_alcohol_percentage_point(alcohol_percentage)
        self.assertEquals(alcohol_percentage_point, 0.5)

        alcohol_percentage = 90
        alcohol_percentage_point = process_alcohol_percentage_point(alcohol_percentage)
        self.assertEquals(alcohol_percentage_point, 1.0)

    def test_views_process_mask_behavior_point_works(self):
        mask_behavior = ['I don\'t usually wear a mask in public.']
        mask_behavior_point = process_mask_behavior_point(mask_behavior)
        self.assertEquals(mask_behavior_point, 0.0)

        mask_behavior = ['I don\'t usually wear a mask in public.', 'I wear masks loosely', 'I don\'t really cover my nose or my mouth with my mask']
        mask_behavior_point = process_mask_behavior_point(mask_behavior)
        self.assertEquals(mask_behavior_point, 0.0)

        mask_behavior = ['I push my mask down to my chin or my neck to eat or drink', 'I wear masks loosely', 'I don\'t really cover my nose or my mouth with my mask']
        mask_behavior_point = float(process_mask_behavior_point(mask_behavior))
        self.assertEquals(mask_behavior_point, 0.4)

        mask_behavior = []
        mask_behavior_point = process_mask_behavior_point(mask_behavior)
        self.assertEquals(mask_behavior_point, 1.0)
    
    
    def test_views_process_sanitize_occasions_point_works(self):
        sanitize_occasions_list = []
        sanitize_occasions_point = process_sanitize_occasions_point(sanitize_occasions_list)
        self.assertEquals(sanitize_occasions_point, 0.0)

        sanitize_occasions_list = ['After coughing or sneezing', 'After using the restroom']
        sanitize_occasions_point = process_sanitize_occasions_point(sanitize_occasions_list)
        self.assertEquals(sanitize_occasions_point, 0.4)

        sanitize_occasions_list = ['After coughing or sneezing', 'After using the restroom', 'Before eating or preparing food', 
        'After contact with animals or pets', 'Before touching my face']
        sanitize_occasions_point = process_sanitize_occasions_point(sanitize_occasions_list)
        self.assertEquals(sanitize_occasions_point, 1.0)
    
    def test_views_proccess_sanitize_wash_time(self):
        sanitize_wash_time = 100
        result = proccess_sanitize_wash_time(sanitize_wash_time)
        self.assertEquals(result, "100 seconds")

        sanitize_wash_time = 1
        result = proccess_sanitize_wash_time(sanitize_wash_time)
        self.assertEquals(result, "1 second")



    def test_views_process_mask_percentage_works(self):
        type_point = 'N95 mask (5-ply mask with firm structure, usually colored in white)'
        behavior_list = ['I don\'t usually wear a mask in public.']
        mask_percentage = process_mask_percentage(type_point, behavior_list)
        self.assertEquals(mask_percentage, 50)

        type_point = 'N95 mask (5-ply mask with firm structure, usually colored in white)'
        behavior_list = ['I wear masks loosely', 'I don\'t really cover my nose or my mouth with my mask']
        mask_percentage = process_mask_percentage(type_point, behavior_list)
        self.assertEquals(mask_percentage, 80)

        type_point = '2-ply cloth mask'
        behavior_list = ['I don\'t really cover my nose or my mouth with my mask',
        'I take my mask off for some time in public',
        'I push my mask down to my chin or my neck to eat or drink',
        'I wear reusable masks multiple times before washing it']
        mask_percentage = process_mask_percentage(type_point, behavior_list)
        self.assertEquals(mask_percentage, 35)

    def test_views_process_pd_percentage_works(self):
        exp = 'I usually distance myself from strangers in public and I\'m pretty sure that most of the time, the distance has always been at least 2 meters (6 ft 6 in) away'
        crowdedness = 'Uncrowded'
        openness = 'Open area'
        pd_percentage = process_pd_percentage(exp, crowdedness, openness)
        self.assertEquals(pd_percentage, 100)

        exp = 'I usually distance myself from strangers in public and I\'m pretty sure that most of the time, the distance has always been at least 2 meters (6 ft 6 in) away'
        crowdedness = 'Uncrowded'
        openness = 'Enclosed space'
        pd_percentage = process_pd_percentage(exp, crowdedness, openness)
        self.assertEquals(pd_percentage, 75)

        exp = 'I have never been really distancing myself to strangers at least 2 meters (6 ft 6 in) away or paying attention to the distance'
        crowdedness = 'Uncrowded'
        openness = 'Open area'
        pd_percentage = process_pd_percentage(exp, crowdedness, openness)
        self.assertEquals(pd_percentage, 50)

    def test_views_process_sntz_percentage_works(self):
        wash_hand = 'Yes, I wash my hands regularly'
        soap_type = 'Liquid soap'
        wash_time = 20
        hsanitizer = 'Yes, I sanitize my hands when I don\'t have a proper access to wash them'
        occassions_list = ['After coughing or sneezing', 'After using the restroom', 'Before eating or preparing food', 
        'After contact with animals or pets', 'Before touching my face']
        alcohol_percentage = 80
        disinfect = 'Yes, I usually disinfect things I bring from outside'
        sntz_percentage = process_sanitize_percentage(wash_hand, soap_type, wash_time, 
            hsanitizer, occassions_list, alcohol_percentage, disinfect)   
        self.assertEquals(sntz_percentage, 100)

        wash_hand = 'No, I don\'t wash my hands regularly'
        soap_type = 'No soap'
        wash_time = 10
        hsanitizer = 'No, I don\'t usually sanitize my hands'
        occassions_list = ['After coughing or sneezing', 'After using the restroom', 'Before eating or preparing food']
        alcohol_percentage = 70
        disinfect = 'No, I don\'t disinfect things I bring from outside'
        sntz_percentage = process_sanitize_percentage(wash_hand, soap_type, wash_time, 
            hsanitizer, occassions_list, alcohol_percentage, disinfect)  
        self.assertEquals(sntz_percentage, 25)
    
    def test_simulation_get_ob_overall(self):
        ObedienceSurveyResponse.objects.create(mask_obedience=100, PD_obedience=20, sntz_obedience=50)
        ObedienceSurveyResponse.objects.create(mask_obedience=0, PD_obedience=40, sntz_obedience=80)
        self.assertEquals(get_ob_overall("MASK"), "50.0")
        self.assertEquals(get_ob_overall("PD"), "30.0")
        self.assertEquals(get_ob_overall("SNTZ"), "65.0")

    def test_simulation_get_list_as_p(self):
        null_message = "(No person here)"

        source_list = ["Danan", "Faisal", "Irfan", "Nadila"]
        list_as_p = get_list_as_p(source_list, null_message)
        self.assertEquals(list_as_p, '1. Danan<br>2. Faisal<br>3. Irfan<br>4. Nadila')

        source_list = []
        list_as_p = get_list_as_p(source_list, null_message)
        self.assertEquals(list_as_p, '(No person here)')        