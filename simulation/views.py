from django.core import serializers
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
import json

from math import floor, ceil

from .forms import *
from .models import ObedienceSurveyResponse, get_ob_overall 

def index(request):
    redirect_page = 'simul-index.html'        
    test_form = TestForm(request.POST)
    mask_overall = get_ob_overall('MASK')
    pd_overall = get_ob_overall('PD')
    sntz_overall = get_ob_overall('SNTZ')
        
    context = {
        'test_form':test_form, 
        'mask_percentage':mask_overall,
        'pd_percentage':pd_overall,
        'sanitize_percentage':sntz_overall,
        }
    return render(request, template_name=redirect_page, context=context)

@login_required(login_url="login")
def survey(request):
    mask_form = MaskForm()
    pd_form = PDForm()
    sanitize_form = SanitizeForm()
    
    context = {
        'mask_form': mask_form, 
        'pd_form':pd_form, 
        'sanitize_form':sanitize_form,
    }
    return render(request, "simul-survey.html", context=context)

@login_required(login_url="login")
def score(request):
    mask_form = MaskForm()
    pd_form = PDForm()
    sanitize_form = SanitizeForm()

    if request.method == 'POST':
        if 'sanitize_wash_time' in request.POST :
            mask_form = MaskForm(request.POST)
            pd_form = PDForm(request.POST)
            sanitize_form = SanitizeForm(request.POST)
            if mask_form.is_valid() and pd_form.is_valid() and sanitize_form.is_valid():
                
                mask_type = mask_form.cleaned_data['mask_type_point']

                mask_behavior_list = request.POST.get('mask_behavior_point', [])
                if mask_behavior_list:  
                    mask_behavior_list = json.loads(mask_behavior_list)

                pd_exp = request.POST.get('pd_exp_point')

                pd_area_crowdedness = request.POST.get('pd_area_crowdedness_point')
                pd_area_openness = request.POST.get('pd_area_openness_point')

                sanitize_wash_hand_point = request.POST.get('sanitize_wash_hand_point')
                sanitize_soap_type_point = request.POST.get('sanitize_soap_type_point')
                sanitize_wash_time = int(request.POST.get('sanitize_wash_time'))

                sanitize_occasions_list = request.POST.get('sanitize_occasions_point', [])
                if sanitize_occasions_list:  
                    sanitize_occasions_list = json.loads(sanitize_occasions_list)

                sanitize_hsanitizer_point = request.POST.get('sanitize_hsanitizer_point')
                sanitize_alcohol_percentage = int(request.POST.get('sanitize_alcohol_percentage'))
                sanitize_disinfect_point = request.POST.get('sanitize_disinfect_point')

                mask_percentage = process_mask_percentage(mask_type, mask_behavior_list)
                pd_percentage = process_pd_percentage(pd_exp, pd_area_crowdedness, pd_area_openness)
                sanitize_percentage = process_sanitize_percentage(
                    sanitize_wash_hand_point,
                    sanitize_soap_type_point,
                    sanitize_wash_time, 
                    sanitize_hsanitizer_point,
                    sanitize_occasions_list,
                    sanitize_alcohol_percentage,
                    sanitize_disinfect_point
                    )

                mask_answer = [mask_type, get_list_as_p(mask_behavior_list, '(No habit selected)')]
                mask_response = zip(MASK_LABEL, mask_answer) #nanti ditulis manual di js :')

                pd_area_answer = str(pd_area_crowdedness) + ' ' + str(pd_area_openness).lower()
                pd_answer = [pd_exp, pd_area_answer]
                pd_response = zip(PD_LABEL, pd_answer) #nanti ditulis manual di js :')

                sntz_answer = [
                    sanitize_wash_hand_point, sanitize_soap_type_point, proccess_sanitize_wash_time(sanitize_wash_time),
                    get_list_as_p(sanitize_occasions_list, '(No occasion selected)'), sanitize_hsanitizer_point,
                    str(sanitize_alcohol_percentage) + '%', sanitize_disinfect_point,
                ]
                sntz_response = zip(SANITIZE_LABEL, sntz_answer) #nanti ditulis manual di js :')

                response_instance = ObedienceSurveyResponse.objects.create(
                    mask_obedience = mask_percentage,
                    PD_obedience = pd_percentage,
                    sntz_obedience = sanitize_percentage,
                    mask_type = mask_answer[0],
                    mask_behavior = mask_answer[1],
                    PD_experience = pd_answer[0],
                    PD_area_space = pd_answer[1],
                    sntz_wash_hand = sntz_answer[0],
                    sntz_soap_type = sntz_answer[1],
                    sntz_wash_time = sntz_answer[2],
                    sntz_occasions = sntz_answer[3],
                    sntz_hsanitizer = sntz_answer[4], 
                    sntz_alcohol_percentage = sntz_answer[5],
                    sntz_disinfect = sntz_answer[6]
                )
                
                response_instance.save()

                responses = ObedienceSurveyResponse.objects.all()
                responsesJSON = serializers.serialize('json', responses)
                return HttpResponse(responsesJSON, content_type='application/json')

        mask_form.errors.update(pd_form.errors)
        mask_form.errors.update(sanitize_form.errors)
        main_response = mask_form.errors.as_json()
        response = JsonResponse(main_response, safe=False)
        response.status_code = 403 # To announce that the user isn't allowed to publish
        return response

    #GET
    responses = ObedienceSurveyResponse.objects.all()
    responsesJSON = serializers.serialize('json', responses)
    return HttpResponse(responsesJSON, content_type='application/json')

def get_list_as_p(source_list, null_message):
    last_index = len(source_list) - 1
    if len(source_list) != 0:
        result_p = ''
        for i in range(last_index):
            result_p += '{}. {}<br>'.format(i+1, str(source_list[i]))
        result_p += '{}. {}'.format(last_index+1, str(source_list[last_index]))
        return result_p
    
    else: 
        return null_message

def process_wash_time_point(wash_time):
    wash_time = int(wash_time)
    if wash_time >= 20 :
        return 1.0
    else :
        return wash_time / 20

def process_alcohol_percentage_point(alcohol_percentage):
    alcohol_percentage = int(alcohol_percentage)
    if alcohol_percentage < 60 :
        return 0.0
    elif alcohol_percentage < 80 :
        return (alcohol_percentage - 60) / 20
    else :
        return 1.0

def process_mask_behavior_point(behavior_list):
    behavior_point = 1.0
    for behavior in behavior_list :
        for mask_behavior_point in MASK_BEHAVIOR_POINT:
            if behavior == mask_behavior_point[1]:
                behavior_point += float(mask_behavior_point[0])

    behavior_point = round(behavior_point, 2)

    if behavior_point < 0.0 :
        return 0.0
    else :
        return behavior_point

def proccess_sanitize_wash_time(sanitize_wash_time):
    if sanitize_wash_time != 1 :
        unit_time = ' seconds'
    else :
        unit_time = ' second'
    return str(sanitize_wash_time) + unit_time

def process_sanitize_occasions_point(occasions_list):
    occasions_point = 0.0
    for occasions in occasions_list :
        for sanitize_occasions_point in SANITIZE_OCCASIONS_POINT :
            if occasions == sanitize_occasions_point[1]:
                occasions_point += float(sanitize_occasions_point[0])
    return occasions_point

def process_mask_percentage(mask_type, behavior_list):
    type_point = 0.0
    behavior_point = 0.0

    for mask_mask_point in MASK_TYPE_POINT:
        if mask_type == mask_mask_point[1]: #iterate to get the point
            type_point = float(mask_mask_point[0])

    behavior_point = process_mask_behavior_point(behavior_list)
    float_percentage = (type_point + behavior_point) / 2 * 100
    return floor(float_percentage)

def process_pd_percentage(exp, crowdedness, openness):
    exp_point = 0.0
    crowdedness_point = 0.0
    openness_point = 0.0

    for pd_exp_point in PD_EXP_POINT:
        if exp == pd_exp_point[1]:
            exp_point = float(pd_exp_point[0])

    for pd_crowdedness_point in PD_CROWDED_AREA_POINT:
        if crowdedness == pd_crowdedness_point[1]:
            crowdedness_point = float(pd_crowdedness_point[0])

    for pd_openness_point in PD_OPEN_AREA_POINT:
        if openness == pd_openness_point[1]:
            openness_point = pd_openness_point[0]

    float_percentage = (exp_point + crowdedness_point + openness_point) / 2 * 100
    return floor(float_percentage)

def process_sanitize_percentage(wash_hand, soap_type, wash_time, 
    hsanitizer, occasions_list, alcohol_percentage, disinfect) :

    wash_hand_point = 0.0
    soap_type_point = 0.0
    hsanitizer_point = 0.0
    disinfect_point = 0.0

    wash_time_point = process_wash_time_point(wash_time)
    occasions_point = process_sanitize_occasions_point(occasions_list)
    alcohol_percentage_point = process_alcohol_percentage_point(alcohol_percentage)

    for sanitize_wash_hand_point in SANITIZE_YES_NO_WASH_HAND_POINT:
        if wash_hand == sanitize_wash_hand_point[1]:
            wash_hand_point = sanitize_wash_hand_point[0]
    
    for sanitize_soap_type_point in SANITIZE_SOAP_TYPE_POINT:
        if soap_type == sanitize_soap_type_point[1]:
            soap_type_point = sanitize_soap_type_point[0]
    
    for sanitize_hsanitizer_point in SANITIZE_YES_NO_H_SANITIZER_POINT:
        if hsanitizer == sanitize_hsanitizer_point[1]:
            hsanitizer_point = sanitize_hsanitizer_point[0]
    
    for sanitize_disinfect_point in SANITIZE_YES_NO_DISINFECT_POINT:
        if disinfect == sanitize_disinfect_point[1]:
            disinfect_point = sanitize_disinfect_point[0]

    if soap_type_point == 0.0 :
        wash_hand_point = 0.0
    
    wash_hand_overall = (soap_type_point + wash_hand_point + wash_time_point) / 3
    hsanitizer_overall = (hsanitizer_point + alcohol_percentage_point) / 2

    float_percentage = (wash_hand_overall + hsanitizer_overall + occasions_point + disinfect_point) / 4 * 100
    return floor(float_percentage)