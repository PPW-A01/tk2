from django.db import models
# from django.conf import settings 

# Create your models here.

class ObedienceSurveyResponse(models.Model):
    # associated_survey = models.OneToOneField(OPForm, on_delete=models.CASCADE, primary_key=True, default=get_opform)
    mask_obedience = models.IntegerField(default=50)
    PD_obedience = models.IntegerField(default=50)
    sntz_obedience = models.IntegerField(default=50)
    mask_type = models.CharField(max_length=1000, default='3-ply cloth mask')
    mask_behavior = models.CharField(max_length=1000, default='(No habit selected)')
    PD_experience = models.CharField(max_length=1000, default='I have tried distancing myself from strangers but I don\'t think it was 2 meters (6 ft 6 in) away or more')
    PD_area_space = models.CharField(max_length=1000, default='Uncrowded open area')
    sntz_wash_hand = models.CharField(max_length=1000, default='Yes, I wash my hands regularly')
    sntz_soap_type = models.CharField(max_length=1000, default='Bar soap')
    sntz_wash_time = models.CharField(max_length=1000, default='20 seconds') #fixed string
    sntz_occasions = models.CharField(max_length=1000, default='(No occasion selected)')
    sntz_hsanitizer = models.CharField(max_length=1000, default='Yes, I sanitize my hands when I don\'t have a proper access to wash them') 
    sntz_alcohol_percentage = models.CharField(max_length=1000, default='70%') #fixed string
    sntz_disinfect = models.CharField(max_length=1000, default='Yes, I usually disinfect things I bring from outside')

def get_ob_overall(ob_type):
    ob_overall_sum_float = 0.0
    ob_percentage_list = ObedienceSurveyResponse.objects.all()
    ob_percentage_count = ob_percentage_list.count()

    if ob_type == 'MASK' :
        for obp in ob_percentage_list:
            ob_overall_sum_float += obp.mask_obedience
    
    elif ob_type == 'PD':
        for obp in ob_percentage_list:
            ob_overall_sum_float += obp.PD_obedience

    elif ob_type == 'SNTZ' :
        for obp in ob_percentage_list:
            ob_overall_sum_float += obp.sntz_obedience

    try:
        ob_overall_mean = ob_overall_sum_float / ob_percentage_count

    except ZeroDivisionError:
        ob_overall_mean = 0

    return str('%.1f' %  ob_overall_mean)