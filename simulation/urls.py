from django.urls import path

from . import views

app_name = 'simulation'

urlpatterns = [
    path('', views.index, name='simulation'),
    path('score/', views.score, name='simulation-score'),
    path('survey/', views.survey, name='simulation-survey')
]