from .models import Feedback
from .forms import FeedbackForm
from django.core import serializers
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

# Create your views here.
def feedback(request):
    if request.user.is_authenticated:
        form = FeedbackForm()
        
        content = {
            'form' : form
        }
        return render(request, 'TeKa/feedback.html', content)
    
    return render(request, 'TeKa/beforeLogin.html')

@login_required(login_url = 'login')
def feedbackResult(request):
    form = FeedbackForm()
    if request.method == 'POST':
        form = FeedbackForm(request.POST)
        if form.is_valid():
            form.save()
            
    data = Feedback.objects.all()
    dataJson = serializers.serialize('json', data)
    return HttpResponse(dataJson, content_type='application/json')

@login_required(login_url = 'login')
def feedbackCollection(request):
    if request.user.is_authenticated:
        data = Feedback.objects.all()
        content = {
            'data' : data
        }
        
        return render(request, 'TeKa/feedback-collection.html', content)

@login_required(login_url = 'login')
def deleteFeedback(request, pk):
    data = Feedback.objects.get(id = pk)
    if request.method == 'POST':
        data.delete()
        return redirect('feedbackCollection')
    
    return render(request, 'TeKa/delete-feedback.html')
