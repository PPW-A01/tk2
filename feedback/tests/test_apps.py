from django.apps import apps
from django.test import TestCase
from feedback.apps import FeedbackConfig

class FeedbackConfigTest(TestCase):
    def test_apps(self):
        self.assertEqual(FeedbackConfig.name, 'feedback')
        self.assertEqual(apps.get_app_config('feedback').name, 'feedback')
