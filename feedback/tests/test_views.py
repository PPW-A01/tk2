from http import HTTPStatus as stats
from feedback.models import Feedback
from feedback.forms import FeedbackForm
from django.test import TestCase, Client
from django.urls import resolve, reverse

# Create your tests here.
class feedbackViewsTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.client = Client()
        cls.test_feedback = Feedback.objects.create(
            email = 'kelompokppw.a01@protonmail.com',
            feedback_description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
        )
        cls.feedback_url = '/feedback/'
        cls.feedbackJson = '/feedback/feedbackResult/'
        cls.feedbackCollection_url = '/feedback/collection/'
        cls.deleteFeedback_url = '/feedback/deleteFeedback/%d/' % cls.test_feedback.id
        cls.register = '/register/'
        cls.login = '/login/'
        cls.logout = '/logout/'

    def test_feedback_url_GET(self):
        response = self.client.get(self.feedback_url)
        self.assertTemplateUsed(response, 'TeKa/beforeLogin.html')
        self.assertEqual(response.status_code, stats.OK)

    def test_feedbackJson_GET(self):
        usercreate = self.client.post(self.register, {
            'username': 'faisaladisoe',
            'email': 'muhammad.faisal95@ui.ac.id',
            'password1': 'ayokitajoggingdulu',
            'password2': 'ayokitajoggingdulu'
        }, follow = True)
        usercreate.client.login(username='faisaladisoe', password='ayokitajoggingdulu')
        response = self.client.get(self.feedbackJson)
        self.assertEqual(response.status_code, stats.OK)

    def test_feedback_collection_url_GET(self):
        usercreate = self.client.post(self.register, {
            'username': 'faisaladisoe',
            'email': 'muhammad.faisal95@ui.ac.id',
            'password1': 'ayokitajoggingdulu',
            'password2': 'ayokitajoggingdulu'
        }, follow = True)
        usercreate.client.login(username='faisaladisoe', password='ayokitajoggingdulu')
        response = self.client.get(self.feedbackCollection_url, follow = True)
        self.assertTemplateUsed(response, 'TeKa/feedback-collection.html')
        self.assertEqual(response.status_code, stats.OK)
    
    def test_delete_feedback_url_GET(self):
        usercreate = self.client.post(self.register, {
            'username': 'faisaladisoe',
            'email': 'muhammad.faisal95@ui.ac.id',
            'password1': 'ayokitajoggingdulu',
            'password2': 'ayokitajoggingdulu'
        }, follow = True)
        usercreate.client.login(username='faisaladisoe', password='ayokitajoggingdulu')
        response = self.client.get(self.deleteFeedback_url, follow = True)
        self.assertTemplateUsed(response, 'TeKa/delete-feedback.html')
        self.assertEqual(response.status_code, stats.OK)
    
    def test_feedback_url_POST(self):
        usercreate = self.client.post(self.register, {
            'username': 'faisaladisoe',
            'email': 'muhammad.faisal95@ui.ac.id',
            'password1': 'ayokitajoggingdulu',
            'password2': 'ayokitajoggingdulu'
        }, follow = True)
        usercreate.client.login(username='faisaladisoe', password='ayokitajoggingdulu')
        responseForm = self.client.get(self.feedback_url, follow = True)
        self.assertTemplateUsed(responseForm, 'TeKa/feedback.html')
        self.assertEqual(responseForm.status_code, stats.OK)

    def test_feedbackJson_POST(self):
        usercreate = self.client.post(self.register, {
            'username': 'faisaladisoe',
            'email': 'muhammad.faisal95@ui.ac.id',
            'password1': 'ayokitajoggingdulu',
            'password2': 'ayokitajoggingdulu'
        }, follow = True)
        usercreate.client.login(username='faisaladisoe', password='ayokitajoggingdulu')
        response = self.client.post(self.feedbackJson, {
            'email' : 'kelompokppw.a01@protonmail.com',
            'feedback_description' : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
        }, follow = True)
        self.assertEqual(response.status_code, stats.OK)
    
    def test_delete_feedback_url_POST(self):
        usercreate = self.client.post(self.register, {
            'username': 'faisaladisoe',
            'email': 'muhammad.faisal95@ui.ac.id',
            'password1': 'ayokitajoggingdulu',
            'password2': 'ayokitajoggingdulu'
        }, follow = True)
        usercreate.client.login(username='faisaladisoe', password='ayokitajoggingdulu')
        response = self.client.post(self.deleteFeedback_url, follow = True)
        self.assertEqual(response.status_code, stats.OK)
        self.assertRedirects(response, '/feedback/collection/')
        
