$(document).ready(() => {
    $.ajax({
        type: 'GET',
        url: '/feedback/feedbackResult/',
        success: data => {
            $('.row').empty();
            let tempContainer = data;
            tempContainer.map(item => {
                let userEmail = item.fields.email;
                let userFeedback = item.fields.feedback_description;
                $('.row').append(
                    `<div class="col-sm col-md-*">
                        <div class="card" style="width: 18rem;">
                            <div class="card-body">
                                <h5 class="card-title">${userEmail}</h5>
                                <h6 class="card-subtitle mb-2 text-muted">${userFeedback}</h6>
                            </div>
                        </div>
                    </div>`
                );
            });
        }
    });

    $('#submit-button').on('click', e => {
        e.preventDefault();
        const user_email = $('#user-email').val();
        const user_feedback = $('#user-feedback').val();
        const csrfToken = $('input[name = csrfmiddlewaretoken]').val();
        if (user_email === '' || user_feedback === '') {
            $('#user-email').val('');
            $('#user-feedback').val('');
            $('#feedback-tagline').text('Sorry we can\'t process your feedback');
            $('#feedback-tagline').css({
                'color' : '#ef233c',
            });
            $('#feedback-secondTagline').text('Perhaps either the email or the feedback field is still blank');
        } else {
            $.ajax({
                type: 'POST',
                url: '/feedback/feedbackResult/',
                data: {
                    csrfmiddlewaretoken: csrfToken,
                    email: user_email,
                    feedback_description: user_feedback,
                },
                success: data => {
                    $('.row').empty();
                    $('#user-email').val('');
                    $('#user-feedback').val('');
                    $('#feedback-tagline').text('Congrats your feedback successfully added!');
                    $('#feedback-secondTagline').text('Thank you for your feedback!');
                    let tempContainer = data;
                    tempContainer.map(item => {
                        let userEmail = item.fields.email;
                        let userFeedback = item.fields.feedback_description;
                        $('.row').append(
                            `<div class="col-sm col-md-*">
                                <div class="card" style="width: 18rem;">
                                    <div class="card-body">
                                        <h5 class="card-title">${userEmail}</h5>
                                        <h6 class="card-subtitle mb-2 text-muted">${userFeedback}</h6>
                                    </div>
                                </div>
                            </div>`
                        );
                    });
                }
            });
        }
    });

    $('#think-word').on('mouseover', () => {
        $('#think-word').text('What\'s your thoughts on our website?');
        $('#think-word').css({
            'background-color' : '#f6ecf0',
        });
    });

    $('#think-word').on('mouseout', () => {
        $('#think-word').text('What do you think about this website?');
        $('#think-word').css({
            'background-color' : '',
        });
    });
});