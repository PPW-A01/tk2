from django import forms
from .models import Fiturku

class Fiturform(forms.ModelForm):

    class Meta :
        model = Fiturku
        fields = '__all__'
        
        tips = forms.CharField(label="Tips", widget=forms.TextInput(attrs={
            "id" : "tips",
            "class" : "form-control mb-3",
           
        }))

        description = forms.CharField(label="Description", widget=forms.TextInput(attrs={
            "id" : "description",
            "class" : "form-control mb-3",
            
        }))
        
       