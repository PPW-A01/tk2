from django.test import TestCase,Client
from django.urls import resolve, reverse
from django.http import HttpRequest, response
from fitur.apps import FiturConfig
from django.apps import apps

from .models import Fiturku
from .views import *

# Create your tests here.

class TestTips_Views(TestCase):
    def setUp(self):
        self.client = Client()
        self.tipsForm = '/tips/tipsform/'
        self.fiturTipsUrl = '/tips/'
        self.register = '/register/'
        self.login = '/login/'
        self.logout = '/logout/'

    def test_url_tips_exist(self):
        response= Client().get('/tips/')
        self.assertEqual(response.status_code, 200)

    def test_memuat_model_fiturtips(self):
        fitur = Fiturku.objects.create(tips = "memakai masker")
        count = Fiturku.objects.all().count()
        self.assertEqual(str(fitur), "memakai masker")
        self.assertEqual(count, 1)

    """
    def test_form_tips_post(self):
        usercreate = self.client.post(self.register, {
            'username': 'nazwa',
            'email': 'nazwasyarifah12@gmail.com',
            'password1': '12345nazwa',
            'password2': '12345nazwa'
        }, follow = True)
        usercreate.client.login(username='nazwa', password='12345nazwa')
        responseGET = self.client.get(self.tipsForm, follow = True)
        responsePOST = self.client.post(self.tipsForm, {
            'tips' : 'jogging setiap pagi'
        }, follow = True)
        self.assertEqual(responseGET.status_code, 200)
        self.assertEqual(responsePOST.status_code, 200)
        self.assertRedirects(responsePOST, '/tips/')
    """
class TestTips(TestCase):
    def setUp(self):
        Fiturku.objects.create(tips = "mencuci tangan")

    def test_url_tips_exist(self):
        response= Client().get('/tips/')
        self.assertEqual(response.status_code, 200)

    def test_url_guide_exist(self):
        response= Client().get('/tips/guide')
        self.assertEqual(response.status_code, 200)
    """
    def test_url_form_exist(self):
        response = Client().get('/tips/tipsform/')
        self.assertEqual(response.status_code, 302)
    """
    def test_memuat_model_fiturtips(self):
        fitur = Fiturku.objects.create(tips = "memakai masker")
        count = Fiturku.objects.all().count()
        self.assertEqual(count,2)

    def test_model_name(self):
        tips = Fiturku.objects.all()[0]
        self.assertEqual(str(tips), "mencuci tangan")
    '''
    def test_add_tips_from_page(self):
        count = Fiturku.objects.all().count()
        response = Client().post('/tips/tipsform/', data= {'tips': 'menjaga jarak'})
        self.assertEqual(Fiturku.objects.all().count(), 1)
        self.assertEqual(response.status_code, 302)
    '''

class FiturApp(TestCase):
    def test_apps(self):
        self.assertEqual(FiturConfig.name, 'fitur')
        self.assertEqual(apps.get_app_config('fitur').name, 'fitur')

class ModelTest(TestCase):
    @classmethod
    def setUp(cls):
        cls.test_tips = Fiturku.objects.create(
            tips = 'memakai masker',
            description = 'usahakan selalu memakai masker jika berada di luar rumah'
        )

    def test_description(self):
        description = Fiturku.objects.get(id = 1)
        max_length = description._meta.get_field('description').max_length
        self.assertEqual(max_length, 1000)
    