from django.db import models

# Create your models here.
class Fiturku(models.Model):
    tips = models.CharField(max_length = 500, null=True)
    description = models.TextField(max_length=1000, null=True)

    def __str__(self):
        return self.tips
