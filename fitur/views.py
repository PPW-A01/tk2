from django.shortcuts import render,redirect
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse,JsonResponse
from django.core import serializers
from .forms import Fiturform
from .models import Fiturku


def tipslist(request):
    form = Fiturform()
    listp = Fiturku.objects.all()
    if request.is_ajax and request.method == 'POST':
        print(request)
        form = Fiturform(request.POST)
        if form.is_valid():
            data = form.save()
            json_tips = serializers.serialize('json', [data, ])
            return JsonResponse({"tips":json_tips},status=200)
        else:
            return JsonResponse({"error": form.errors}, status=400)

    context = {
        "namanya" : request.user.username,
        "listp" : listp,
        "form" : form,
    }
    return render (request, 'fitur/tips_list.html', context)

#@login_required(login_url="login")
def tipsform(request):
    form = Fiturform()
    if request.method == 'POST':
        form = Fiturform(request.POST)
        print(form)
        if form.is_valid():
            data = form.save()
            return JsonResponse(data)

    #return render (request, 'fitur/tips_form.html', {'form':form})

def guide(request):
    return render (request, "fitur/tips_guide.html")