$(document).on('submit', '#form',function(e){
    e.preventDefault();
    var serializedData = $(this).serialize();
    $.ajax({
        type:'POST',
        url:'/tips/',
        data: serializedData,
        success:function(json){
            console.log(json)
            var instance = JSON.parse(json["tips"])[0]["fields"];
            document.getElementById("form").reset();
            $("#tipslist").append( 
                '<div class="card" style="border-radius: 20px;">' +
                '<div class="card-header"> <b>'+ instance["tips"] +'</b></div>' +
                '<div class="card-body" style="margin: auto;">' +
                '<div class="card-text" style="margin: auto; text-align: center;"> <b>' + instance["description"] + '</b></div>' +
                '</div></div>'
            )
        },
        error : function(response) {
            alert(response["responseJSON"]["error"]);; // provide a bit more info about the error to the console
        }
    });
});



