$(document).ready(function(){

    $("#accordion").accordion();

    function sortByPosition(a, b) {
        var item1pos = Number($(a).attr("position"))
        var item2pos = Number($(b).attr("position"))
        
        if (item1pos < item2pos) return -1;
        if (item2pos > item2pos) return 1;
        return 0;
    }

    function sort_ul_up(clicked_h3, operation) {
        var element_list = $("#accordion").children()
        var ul = $("#accordion")

        var clicked_pos = element_list.index(clicked_h3)
        var clicked_div = element_list[clicked_pos+1]
        if (clicked_pos == 0 && operation == "up") return
        if (clicked_pos == element_list.length-1 && operation == "down") return



        if (operation == "up") {
            var above_h3 = $(element_list[clicked_pos-2])
            var above_div = $(element_list[clicked_pos-1])

            $(clicked_h3).attr("position", above_h3.attr("position")) //Ubah posisi yang di klik
            $(clicked_div).attr("position", above_div.attr("position"))

            above_h3.attr("position", above_h3.attr("position") + 1) //Turunkan posisi yang diatas
            above_div.attr("position", above_div.attr("position") + 1)
        
        } else if (operation == "down") {
            var below_h3 = $(element_list[clicked_pos+2])
            var below_div = $(element_list[clicked_pos+3])

            $(clicked_h3).attr("position", below_h3.attr("position")) //Ubah posisi yang di klik
            $(clicked_div).attr("position", below_div.attr("position"))

            below_h3.attr("position", below_h3.attr("position") - 1) //Turunkan posisi yang diatas
            below_div.attr("position", below_div.attr("position") - 1)
        }

        element_list.sort(sortByPosition)
       
        $.each(element_list, (index, li) => {
            ul.append(li);
        })
    }


    $(".up").click((e) => {
        var lix = e.target.parentElement
        console.log("up")
        sort_ul_up(lix,"up")
    })

    $(".down").click((e) => {
        var lix = e.target.parentElement
        sort_ul_up(lix,"down")
    })

})
