from django import forms
from . import models

class ReportForm(forms.ModelForm):
    class Meta:
        model = models.Report
        fields = '__all__'

    age = forms.IntegerField(label='Age', max_value=120, min_value=1, widget=forms.TextInput(attrs=
    {
        'required' : 'True',
        'placeholder' : 'Age'
    }))

    gender = forms.CharField(label='Gender', widget=forms.Select(attrs=
    {
        'required' : 'True',
    }, choices=(
        ('M', 'Male'),
        ('F', 'Female')
    )))

    status = forms.CharField(label='Status', widget=forms.Select(attrs=
    {
        'required' : 'True',
    }, choices=(
        (None, '---Pick Status---'),
        ('Positive', 'Positive'),
        ('Symptomatic', 'Showing Symptoms'),
        ('Recovered', 'Recovered')
    )))

    province = forms.CharField(label='Province', widget=forms.Select(attrs=
    {
        'required' : 'True',
    }, choices=(
        (None, '---Pick Province---'),
        ('Aceh', 'Aceh'),
        ('Sumatera Utara', 'Sumatera Utara'),
        ('Sumatera Barat', 'Sumatera Barat'),
        ('Riau', 'Riau'),
        ('Jambi', 'Jambi'),
        ('Sumatera Selatan', 'Sumatera Selatan'),
        ('Bengkulu', 'Bengkulu'),
        ('Lampung', 'Lampung'),
        ('Kep. Bangka Belitung', 'Kepulauan Bangka Belitung'),
        ('Kep. Riau', 'Kepulauan Riau'),
        ('DKI Jakarta', 'DKI Jakarta'),
        ('Jawa Barat', 'Jawa Barat'),
        ('Jawa Tengah', 'Jawa Tengah'),
        ('DI Yogyakarta', 'DI Yogyakarta'),
        ('Jawa Timur', 'Jawa Timur'),
        ('Banten', 'Banten'),
        ('Bali', 'Bali'),
        ('Nusa Tenggara Barat', 'Nusa Tenggara Barat'),
        ('Nusa Tenggara Timur', 'Nusa Tenggara Timur'),
        ('Kalimantan Barat', 'Kalimantan Barat'),
        ('Kalimantan Tengah', 'Kalimantan Tengah'),
        ('Kalimantan Selatan', 'Kalimantan Selatan'),
        ('Kalimantan Timur', 'Kalimantan Timur'),
        ('Kalimantan Utara', 'Kalimantan Utara'),
        ('Sulawesi Utara', 'Sulawesi Utara'),
        ('Sulawesi Tengah', 'Sulawesi Tengah'),
        ('Sulawesi Selatan', 'Sulawesi Selatan'),
        ('Sulawesi Tenggara', 'Sulawesi Tenggara'),
        ('Gorontalo', 'Gorontalo'),
        ('Sulawesi Barat', 'Sulawesi Barat'),
        ('Maluku', 'Maluku'),
        ('Maluku Utara', 'Maluku Utara'),
        ('Papua Barat', 'Papua Barat'),
        ('Papua', 'Papua')
    )))

    city = forms.CharField(label='City', widget=forms.TextInput(attrs=
    {
        'required' : 'True',
        'placeholder' : 'City'
    }))