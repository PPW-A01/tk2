from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from maps.forms import ReportForm
from maps.models import Report

def maps(request):
    data = Report.objects.all().order_by('id').reverse()[:10]
    context = {
        'data' : data,
    }
    return render(request, 'maps.html', context)

@login_required(login_url='/login/')
def report(request):
    if request.user.is_authenticated:
        form = ReportForm(request.POST or None)
        if form.is_valid and request.method == 'POST':
            Report.objects.create(age=request.POST['age'], gender=request.POST['gender'], status=request.POST['status'], province=request.POST['province'], city=request.POST['city'])
            return JsonResponse({'data' : 'success'})
        context = {
            'form' : form,
        }
        return render(request, 'report.html', context)

def reports(request):
    data = Report.objects.all()
    context = {
        'data' : data,
    }
    return render(request, 'reports.html', context)

def get_data(request, num_data, num_get):
    upper = num_data
    lower = upper - num_get
    data = list(Report.objects.values())
    data.sort(key=lambda k: k['id'], reverse=True)
    data = data[lower:upper]
    for i in data:
        i['dt'] = str(Report.objects.get(id=i['id']))
    limit = True if upper >= len(Report.objects.all()) else False
    return JsonResponse({'data' : data, 'limit' : limit})
