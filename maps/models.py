from django.db import models
from django.utils import timezone
import pytz


class Report(models.Model):
    age = models.IntegerField()
    gender = models.CharField(max_length=10)
    status = models.CharField(max_length=20, null=True)
    province = models.CharField(max_length=30, null=True)
    city = models.CharField(max_length=30)
    dt = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        timezone.activate(pytz.timezone("Asia/Jakarta"))
        cur = timezone.get_current_timezone()
        self.dt = cur.normalize(self.dt)
        return self.dt.strftime('%b %d, %Y @ %H:%M')