from django.test import TestCase
from .models import *
from django.contrib.auth.models import User
from datetime import datetime
import pytz

class TestMaps(TestCase):
    def setUp(self):
        Report.objects.create(age=35, gender='M', status='Recovered', province='Jawa Barat', city='Bogor')
        Report.objects.create(age=20, gender='L', status='Showing Symptoms', province='DKI Jakarta', city='Jakarta Selatan')

    def test_add_report(self):
        count = Report.objects.all().count()
        Report.objects.create(age=20, gender='L', status='Positive', province='Jawa Barat', city='Bandung')
        self.assertEqual(Report.objects.all().count(), count+1)

    def test_model_name(self):
        report = Report.objects.all()[0]
        self.assertEqual(str(report), report.dt.strftime('%b %d, %Y @ %H:%M'))

    def test_add_report_from_page_loggedin(self):
        usercreate = self.client.post('/register/', {
            'username': 'TestUser',
            'email': 'TestUser@email.com',
            'password1': 'TestUserPassword',
            'password2': 'TestUserPassword'
        }, follow = True)
        usercreate.client.login(username='TestUser',password='TestUserPassword')
        count = Report.objects.all().count()
        response = self.client.post('/maps/report/', data={'age':19, 'gender':{'choices': ('M')}, 'status':{'choices': ('Showing Symptoms')}, 'province':{'choices': ('Jawa Barat')}, 'city':'Depok'})
        content = response.content.decode('utf-8')
        self.assertEqual(Report.objects.all().count(), count+1)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['content-type'], 'application/json')
        self.assertEqual(eval(content)['data'], 'success')
            
        
    def test_add_report_from_page_not_loggedin(self):
        count = Report.objects.all().count()
        response = self.client.post('/maps/report/', data={'age':19, 'gender':{'choices': ('M')}, 'status':{'choices': ('Showing Symptoms')}, 'province':{'choices': ('Jawa Barat')}, 'city':'Depok'})
        content = response.content.decode('utf-8')
        self.assertEqual(Report.objects.all().count(), count)
        self.assertEqual(response.status_code, 302)

    def test_url_maps_exist(self):
        response = self.client.get('/maps/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'maps.html')

    def test_url_report_exist(self):
        usercreate = self.client.post('/register/', {
            'username': 'TestUser',
            'email': 'TestUser@email.com',
            'password1': 'TestUserPassword',
            'password2': 'TestUserPassword'
        }, follow = True)
        usercreate.client.login(username='TestUser',password='TestUserPassword')
        response = self.client.get('/maps/report/')
        self.assertEqual(response.status_code, 200)

    def test_url_reports_exist(self):
        response = self.client.get('/maps/self-reports/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'reports.html')

    def test_get_data_ajax(self):
        response = self.client.get('/maps/get-data/10/10/')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['content-type'], 'application/json')
        true = True
        false = False
        self.assertTrue(isinstance(eval(content), dict))
