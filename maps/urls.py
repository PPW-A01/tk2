from django.urls import path

from . import views

app_name = 'maps'

urlpatterns = [
    path('', views.maps, name='maps'),
    path('report/', views.report, name='report'),
    path('self-reports/', views.reports, name='self-reports'),
    path('get-data/<int:num_data>/<int:num_get>/', views.get_data, name='get-data'),
]