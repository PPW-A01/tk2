from django.test import TestCase, Client

class TestHome(TestCase):
    def test_url_index_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')